package examenobjetos;

import objetosexamenobjetos.OpelCorsa;
import objetosexamenobjetos.GarajeLlenoException;
import objetosexamenobjetos.Coche;
import objetosexamenobjetos.Concesionario;
import objetosexamenobjetos.ToyotaAuris;
import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class ExamenObjetos {

    public static void main(String[] args) {
        //Declaración de variables
        int opcion = 0;
        int plazas = 0;
        int modeloEscogido;
        int cocheEscogido;
        //Variables booleanas para comprobar si se han introducido valores numericos
        boolean opcionNumerica, plazasNumerica, modeloNumerico, cocheNumerico;
        boolean salir = false;
        String color;
        Coche cocheAux = null;
        String [] modelosDisponibles = {"Toyoya Auris", "Opel Corsa"};
        Scanner input = new Scanner(System.in);
        
        //Creamos e inicializamos el concesionario del tamaño que diga el usuario
        System.out.println("Bienvenido a la plataforma de gestion del concesionario "
                + "CochesCochesCoches");
        System.out.print("¿Cual es el número de plazas del garaje de está sucursal? ");
        do{
            plazasNumerica = true;
            try{
                plazas = input.nextInt();
            }catch(java.util.InputMismatchException ex){
                System.out.print("Por favor introduzca un número entero de plazas: ");
                plazasNumerica = false;
                input.next();
            }
        }while(!plazasNumerica);
        Concesionario concesionario = new Concesionario(plazas);
        
        //Mostramos el menu mientras que el usuario no escoja la opcion salir
        do{
            printMenu(); 
            do{ 
                try{
                    opcion = input.nextInt();
                    opcionNumerica = true;
                }catch(java.util.InputMismatchException ex){
                    System.out.print("Por favor introduzca un número correspondiente"
                        + " a una opción del menu: ");
                    opcionNumerica = false;
                    input.next();
                }
            }while(!opcionNumerica);
            switch (opcion){
                //Opcion mostrar coches
                case 1:
                    if (concesionario.getCochesEnStock() > 0){
                        printStock(concesionario); 
                        do{
                            System.out.print("Si desea conocer los detalles de algún"
                                + " modelo introduzca su número, introduzca 0 "
                                + "para volver al menu principal: ");
                            try{
                                cocheEscogido = input.nextInt();
                                cocheNumerico = true;
                            }catch(java.util.InputMismatchException ex){
                                cocheNumerico = false;
                                cocheEscogido = -1;
                                input.next();
                            }
                        }while(!cocheNumerico || cocheEscogido>concesionario.getCochesEnStock());
                        if (cocheEscogido>0){
                            cocheAux = concesionario.getCoche(cocheEscogido-1);
                            printDetallesCoche(cocheAux);
                        }
                    }else{
                        System.out.println("No hay ningún coche en stock");
                    }
                    break;
                //Opcion añadir coche
                case 2:
                    do{
                        printAniadirCoche(modelosDisponibles);
                        try{
                            modeloEscogido = input.nextInt();
                            modeloNumerico = true;
                        }catch(java.util.InputMismatchException ex){
                            modeloNumerico = false;
                            modeloEscogido = -1;
                            input.next();
                        }
                    }while(!modeloNumerico || (modeloEscogido<0 || (modeloEscogido>modelosDisponibles.length)));
                    System.out.println("Va a añadir un "
                                    +modelosDisponibles[modeloEscogido-1]+" al garaje");
                    System.out.print("¿De que color es? ");
                    color = input.next();
                    switch (modeloEscogido){
                        case 1:
                            cocheAux = new ToyotaAuris(color);
                        break;
                        case 2:
                            cocheAux = new OpelCorsa(color);
                        break;
                    }
                    try {
                        concesionario.aniadirCoche(cocheAux);
                        System.out.println(modelosDisponibles[modeloEscogido-1]+" añadido correctamente.");
                    }catch (GarajeLlenoException ex) {
                        System.out.println(ex.toString());
                    }
                break;
                //Opcion borrar coche
                case 3:
                    if (concesionario.getCochesEnStock() > 0){
                        printStock(concesionario); 
                        do{
                            System.out.print("Introduzca el número del coche que"
                                    + " desea eliminar, introduzca 0 para volver"
                                    + " al menu principal: ");
                            try{
                                cocheEscogido = input.nextInt();
                                cocheNumerico = true;
                            }catch(java.util.InputMismatchException ex){
                                cocheNumerico = false;
                                cocheEscogido = -1;
                                input.next();
                            }
                        }while(!cocheNumerico || cocheEscogido>concesionario.getCochesEnStock());
                        if (cocheEscogido>0){
                            concesionario.quitarCoche(cocheEscogido-1);
                            System.out.println("Coche eliminado");
                        }
                    }else{
                        System.out.println("No hay ningún coche en stock");
                    }
                    break;
                case 4:
                    salir = true;
                    System.out.println("Cerrando la plataforma.");
                break;
                default:
                    System.out.println("Opción desconocida.");
                break;
            }
        }while(!salir);
    }
    
    
    
    //Funciones auxiliares
    
    //Función para imprimir el menu principal
    private static void printMenu(){
        System.out.println();
        System.out.println("<<<<< CochesCochesCoches >>>>>");
        System.out.println("1) Mostrar coches en stock");
        System.out.println("2) Añadir nuevo coche al stock");
        System.out.println("3) Eliminar un coche");
        System.out.println("4) Salir");
        System.out.print("Por favor elija una opción: ");
    }

    //Función para mostrar los modelos con los que trabaja el concesionario
    private static void printAniadirCoche(String[] modelosDisponibles) {
        System.out.print("Modelos disponibles: ");
        for (int i =0; i<modelosDisponibles.length; i++){
            System.out.print((i+1)+"-"+modelosDisponibles[i]+", ");
        }
        System.out.println();
        System.out.print("Por favor elija el modelo que desea añadir: ");
    }

    //Funcion para mostrar el modelo y color de un coche al listar todos
    private static void printCoche(int i, Coche aux) {
        System.out.println((i+1)+"- "+aux.getModelo()+" "+aux.getColor());
    }

    //Función para imprimir los detalles de un coche
    private static void printDetallesCoche(Coche coche) {
        System.out.println(coche.getModelo() + " " + coche.getColor());
        System.out.println("Combustible: "+coche.getCombustible());
        System.out.println("Número de puertas: "+coche.getPuertas());
    }
    
    //Funcion para imprimir los coches en stock
    private static void printStock(Concesionario concesionario){
        Coche cocheAux;
        for (int i=0; i<concesionario.getCochesEnStock(); i++){
            cocheAux = concesionario.getCoche(i);
            printCoche(i, cocheAux);
        }
    }
}